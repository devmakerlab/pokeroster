<?php

namespace Database\Seeders;

use App\Models\Roster;
use Illuminate\Database\Seeder;

class RosterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Roster::factory(5)->create();
    }
}
