<?php

namespace App\Http\Controllers;

use App\Models\Roster;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RostersController extends Controller
{
    public function show(Roster $roster)
    {
        return view('rosters.show', ['roster' => $roster]);
    }

    public function getPokemons(Roster $roster)
    {
        return array_pad($roster->pokemons, 6, ['id' => null, 'name' => null]);
    }

    public function store(Request $request)
    {
        $roster = Roster::create($request->only('name') + ['reference' => Str::uuid()]);

        return redirect()->route('rosters.edit', $roster);
    }

    public function edit(Roster $roster)
    {
        return view('rosters.edit', ['roster' => $roster]);
    }

    public function update(Roster $roster, Request $request)
    {
        $pokemons = [];

        for($i = 0; $i < 6; $i++) {
            if (is_null($request->get('pokemons_'.$i.'_id'))) {
                continue;
            }

            $pokemons[$i] = [
                'id' => $request->get('pokemons_'.$i.'_id'),
                'name' => $request->get('pokemons_'.$i.'_name'),
            ];
        }

        $roster->update([
            'name' => $request->get('name'),
            'pokemons' => $pokemons,
        ]);

        return redirect()->route('rosters.edit', $roster);
    }
}
