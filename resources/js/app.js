require('./bootstrap');

import Vue from "vue"

Vue.component(
    "show-roster",
    require("./components/ShowRoster.vue").default
)

const app = new Vue({
    el : "#app"
});
