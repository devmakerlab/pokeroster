@extends('layouts.admin')

@section('content')
    <h1 class="text-3xl text-black pb-6">{{ __('Edit roster') }}</h1>
    <form class="p-10 bg-white rounded shadow-lg" method="POST" action="{{ route('rosters.update', $roster) }}">
        @csrf
        @method('patch')
        <div>
            <x-label for="link" :value="__('Link')" />
            <x-input id="link" class="block mt-1 w-full bg-gray-100" disabled name="link" type="text" :value="route('rosters.show', $roster)"/>
        </div>

        <div class="mt-4">
            <x-label for="name" :value="__('Roster\'s name')" />
            <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name', $roster->name)"/>
        </div>

        <?php for($i = 0; $i < 6; $i++): ?>
            <div class="flex flex-wrap mt-4 -mx-2 space-y-4 md:space-y-0">
                <div class="w-full px-2 md:w-1/6">
                    <x-label for="name" :value="__('#'.$i.' ID')" />
                    <x-input id="name" class="block mt-1 w-full" type="text" name="pokemons.{{ $i }}.id" :value="old('pokemons.'.$i.'.id', isset($roster->pokemons[$i]) ? $roster->pokemons[$i]['id'] : null)"/>
                </div>
                <div class="w-full px-2 md:w-5/6">
                    <x-label for="name" :value="__('#'.$i.' Name')" />
                    <x-input id="name" class="block mt-1 w-full" type="text" name="pokemons.{{ $i }}.name" :value="old('pokemons.'.$i.'.name', isset($roster->pokemons[$i]) ? $roster->pokemons[$i]['name'] : null)"/>
                </div>
            </div>
        <?php endfor; ?>

        <x-button>
            {{ __('Submit') }}
        </x-button>
    </form>
@endsection
