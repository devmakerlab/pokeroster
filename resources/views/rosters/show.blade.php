<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ __('DevMaker - Pokeroster') }}</title>

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link href="https://fonts.googleapis.com/css2?family=Bangers:wght@400;700&display=swap" rel="stylesheet">

        <style>
            body {
                font-family: 'Bangers', sans-serif;
            }
        </style>
    </head>
    <body>
        <div id="app">
            <show-roster :roster="{{ $roster->toJson() }}"></show-roster>
        </div>
        <script src="{{ asset('js/app.js') }}" ></script>
    </body>
</html>
