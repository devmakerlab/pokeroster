<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/r/{roster}', [\App\Http\Controllers\RostersController::class, 'show'])->name('rosters.show');
Route::post('/rosters', [\App\Http\Controllers\RostersController::class, 'store'])->name('rosters.store');
Route::get('/rosters/edit/{roster}', [\App\Http\Controllers\RostersController::class, 'edit'])->name('rosters.edit');
Route::patch('/rosters/{roster}', [\App\Http\Controllers\RostersController::class, 'update'])->name('rosters.update');
Route::get('/ajax/rosters/{roster}', [\App\Http\Controllers\RostersController::class, 'getPokemons']);
